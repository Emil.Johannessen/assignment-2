#include <pthread.h> // pthread
#include <stdio.h>   // prinftf
#include <stdlib.h>  // random
#include <unistd.h>  // sleep
#include <semaphore.h>
#include <assert.h>
// you might need more header files

void *worker_actions(void *employee_id);
void *customer_actions(void *personal_id);

int NUM_CUSTOMERS = 5;
int NUM_WORKERS = 2;
int firstConsumer;
int consumerQueue = 0;
int workersAwake = 0;

sem_t costumers;
sem_t workers;
sem_t lock;
sem_t queue;


void Sem_init(sem_t *wrapper, int v){ // error checking the semaphore
    int rc = sem_init(wrapper, 0, v);
    assert( rc == 0);
}

int main(int argc, char **argv) {

    sem_init(&costumers, 0, 0); // initializing costumer-semaphore thread, setting value to 0 
    sem_init(&workers, 0,0); // initializing worker-semaphore thread, setting to value to 0.
    sem_init(&lock, 0,1); // initializing lock-semaphore thread, setting to value to 1.
    sem_init(&queue, 0, 1); // initializing queue-semaphore thread, setting to value to 1.

    
    int personal_ids[NUM_CUSTOMERS], employee_ids[NUM_WORKERS];

    pthread_t customers[NUM_CUSTOMERS], workers[NUM_WORKERS];

    // create treads
    for (int i = 0; i < NUM_WORKERS; i++) {
        employee_ids[i] = i;
        pthread_create(&workers[i], NULL, worker_actions, (void *)&employee_ids[i]);
    }
    for (int i = 0; i < NUM_CUSTOMERS; i++) {
        personal_ids[i] = i;
        pthread_create(&customers[i], NULL, customer_actions, (void *)&personal_ids[i]);
    }

    // join threads
    for (int i = 0; i < NUM_WORKERS; i++) {
        pthread_join(workers[i], NULL);
    }
    for (int i = 0; i < NUM_CUSTOMERS; i++) {
        pthread_join(customers[i], NULL);
    }

    return 0;
}

//helper method
void iceCream(int id) {
    firstConsumer = id;
    int r = rand() % 2;
    sem_wait(&lock);
    if (sem_getvalue(&workers, &workersAwake) == 0) {
        printf("Person %d wake up worker %d.\n", id, r);
        workersAwake++; 
    }
    sem_post(&lock); 
    sem_post(&workers); // wakes up the worker
    sem_wait(&costumers); // waites for the doctor

}

//helper method
void gettingInQueue(int id)  {
    consumerQueue++;
    printf("Person %d gets in queue. %d other waiting.\n", id, consumerQueue);
}

//helper method
void exitingQueue() {
    consumerQueue--;
}

void *worker_actions(void *employee_id) {
    // get the id of this employee
    int id = *(int *)employee_id;

    while (1) {
        if (consumerQueue == 0) {
            printf("No customers, worker %d sleeps.\n", id);
            sem_post(&lock);
        }
        
        sem_wait(&workers); //Sleep until further notice
        int time = rand() % 10;
        printf("Worker %d is making ice cream for person %d. Taking 6 seconds.\n", id, firstConsumer);
        sleep(time);
        printf("Person %d received ice cream.\n", firstConsumer);
    
        sem_post(&costumers);
        sem_wait(&lock);

        
        sem_post(&lock);
    }
}

void *customer_actions(void *personal_id) {
    // get the id of this customer
    int id = *(int *)personal_id;

    while (1) {
        int time = rand() % 10;
        printf("Person %d is relaxing on the beach for %d seconds.\n", id, time);
        sleep(time);

        sem_wait(&lock);
        if (consumerQueue >= 3) { // critical
            printf("Person %d thinks the line is too long, goes to sleep for a while instead.\n", id);
            sleep(time);
            sem_post(&lock);
            continue;
        }
        sem_post(&lock);
        sem_wait(&lock);

        gettingInQueue(id); //critical from, entering the queue
        sem_post(&lock);
        iceCream(id); // critical
        sem_wait(&lock);
        exitingQueue(); // critical
        sem_post(&lock);

        

    
    }
}
